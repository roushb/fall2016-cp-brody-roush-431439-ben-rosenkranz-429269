//Some Code From: https://github.com/hwz/chirp

var express = require('express');
var router = express.Router();
var mongoose = require( 'mongoose' );
//var Post = mongoose.model('Post');
var DailyLogModel = mongoose.model('dailyLogModel');
//Used for routes that must be authenticated.
function isAuthenticated (req, res, next) {
	// if user is authenticated in the session, call the next() to call the next request handler 
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects

	//allow all get request methods
	if(req.method === "GET"){
		return next();
	}
	if (req.isAuthenticated()){
		return next();
	}

	// if the user is not authenticated then redirect him to the login page
	return res.redirect('/#login');
};

//Register the authentication middleware
router.use('/todaysRun', isAuthenticated);
router.use('/todaysRun/:dateString', isAuthenticated);

router.route('/todaysRun')
	//To create a new run
	.post(function(req, res){
        console.log('Is it here???');
        //NOT SURE WHERE THE .body comes from???????
		var log = new DailyLogModel();
		log.username = req.body.username;
		log.mileage = req.body.mileage;
        log.timeMinutes = req.body.timeMinutes;
        log.description = req.body.description;
        log.date = req.body.date;
        console.log(req.body.date);
        //Saves to the mongoDB database, if error sends 500 error        
        log.save(function(err, log) {
			if (err){
				return res.send(500, err);
			}
			console.log('No error');
			return res.json(log);
		});
	})
	
	//To get all runs
	.get(function(req, res){
		console.log('debug1');
		DailyLogModel.find(function(err, logs){
			console.log('debug2');
			if(err){
				return res.send(500, err);
			}
			console.log('print');
			console.log(logs);
			return res.send(200,logs);
		});
	});

//run-specific commands
router.route('/todaysRun/:fullDate/:username?')
	
	.post(function(req, res){
        console.log('Is it here version 2....');
        //NOT SURE WHERE THE .body comes from???????
		var log = new DailyLogModel();
		log.username = req.body.username;
		log.mileage = req.body.mileage;
        log.timeMinutes = req.body.timeMinutes;
        log.description = req.body.description;
        log.date = req.body.date;
        console.log(req.body.date);
        //Saves to the mongoDB database, if error sends 500 error        
        log.save(function(err, log) {
			if (err){
				return res.send(500, err);
			}
			console.log('No error');
			return res.json(log);
		});
	})

	//gets specified run
	.get(function(req, res){
		console.log('WE ARE ACTUALLY HERE THIS TIME');
        //log.findOne({ 'date' :  date }
        console.log(req.params.fullDate);
        console.log(req.params.username);
		DailyLogModel.find({"date":req.params.fullDate, "username":req.params.username}, function(err, log){
			if(err){
				console.log('shit');
				console.error(err);
				res.send(err);
			}else{
			console.log('poop');
			console.log(log);
			return res.json(log);
			}
			
		});
	}) 
	//updates specified run
	.put(function(req, res){
		DailyLogModel.findById(req.params.id, function(err, log){
			if(err)
				res.send(err);
//
		log.username = req.body.username;
		log.mileage = req.body.mileage;
        log.timeMinutes = req.body.timeMinutes;
        log.description = req.body.description;
        log.date = req.body.date;

			log.save(function(err, log){
				if(err)
					res.send(err);
				res.json(log);
			});
		});
	})
	//deletes the run
	.delete(function(req, res) {
		DailyLogModel.remove({
			_id: req.params.id
		}, function(err) {
			if (err)
				res.send(err);
			res.json("deleted run from " + log.date);
		});
	});

router.route('/todaysRun/:fullDate/:id')
	.get(function(req, res){
		DailyLogModel.find({"_id":req.params.id}, function(err, log){
			if(err){
				console.log('shit');
				console.error(err);
				res.send(err);
			}else{
			console.log('single log gotten');
			console.log(log);
			return res.json(log);
			}
			
		});
	})
	//updates specified run
	.put(function(req, res){
		DailyLogModel.findById(req.params.id, function(err, log){
			if(err)
				res.send(err);
//
		log.username = req.body.username;
		log.mileage = req.body.mileage;
        log.timeMinutes = req.body.timeMinutes;
        log.description = req.body.description;
        log.date = req.body.date;

			log.save(function(err, log){
				if(err)
					res.send(err);
				res.json(log);
			});
		});
	})
	.delete(function(req, res) {
		DailyLogModel.remove({
			_id: req.params.id
		}, function(err) {
			if (err)
				res.send(err);
			res.json("deleted run");
		});
	});


module.exports = router;