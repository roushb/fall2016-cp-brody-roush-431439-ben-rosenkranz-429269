<!DOCTYPE html>
<html>
<head>
    <link href="news_css.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <title>My Calendar</title>
    <script src="jquery-3.1.1.min.js"></script>
    <script src="calendar.min.js"></script>
    <script>
	$(document).ready(function(){
		$('#signOut').hide();
		//Token to be used for form validation
		var token = '';
		//username to be used as a check for overlaying events
		var username = '';
		var date = new Date();
		var currentMonth = new Month(date.getFullYear(), date.getMonth());
		/*
		UpdateCalendar() function handles the changes in months
		*/
		function UpdateCalendar(){
			//Empty the calendar to replace everything in there 
			$('#calendar').empty();
			var weeksArr = currentMonth.getWeeks();
			
			//Puts the current month and year up top
			var year = currentMonth.getDateObject(date.getDate()).getFullYear();
			var locale = "en-us";
			var month = currentMonth.getDateObject(1).toLocaleString(locale, { month: "long" });
			document.getElementById("currentMonthAndYear").innerHTML = '<strong>' + month + ' ' + year + '</strong>';
			
			//Iterate through the rows of the calendar, insert dates as appropriate
			for(var i = 0; i < weeksArr.length; i++){
				//This gets the days of the week from the calendar.min.js
				var daysForTable = weeksArr[i].getDates();
				var row = document.getElementById('calendar').insertRow(i);
				//This gets the table from the html that will allow us to insert in the days of the week
				for(var j = 0; j < daysForTable.length; j++){
					var monthForFullDate = daysForTable[j].getMonth() + 1;
					var fullDate = monthForFullDate + '/' + daysForTable[j].getDate() + '/' + daysForTable[j].getFullYear();
					//Insert a hidden field into every td that contains the date represented by that td - will be used to add and delete events
					row.insertCell(j).innerHTML = daysForTable[j].getDate() + '<div class = "hidden">'+fullDate+'</div>';	
				}
			}	
		}
		/*
		The OverlayEvents() method handles the calendar visual - it places events in the appropriate date boxes
		*/
		function OverlayEvents(){
			$(".eventsMeeting, .eventsCall, .eventsPersonal, .eventShared").remove();
			//Load events for the month
			$.post('loadEvents.php',function(data3){
				var response = data3;
				var sepEvents = response.split('$');
				for(var i = 0; i < sepEvents.length-1; i++){
					var eventDetailsArr = sepEvents[i].split('#');
					//Read in the events, change the css if they are shared, insert hidden eventid field within the div to use for event deletion
					if(eventDetailsArr[7] == 'shared' && eventDetailsArr[8] == 'Meeting'){
						$(".hidden:contains("+eventDetailsArr[2]+")").after("<div class = \"eventShared\" name=\"Meeting\"><strong>Event Name:</strong>"+eventDetailsArr[3]+
						"<br><strong>Event Time:</strong> "+eventDetailsArr[4]+
						"<br><strong>Event Desc:</strong> "+eventDetailsArr[5].substring(0,10)+
						"...<input type=\"hidden\" id=\"eventid\" value="+eventDetailsArr[6]+"</div>");
					}
					else if(eventDetailsArr[7] == 'shared' && eventDetailsArr[8] == 'Call'){
						$(".hidden:contains("+eventDetailsArr[2]+")").after("<div class = \"eventShared\" name=\"Call\"><strong>Event Name:</strong>"+eventDetailsArr[3]+
						"<br><strong>Event Time:</strong> "+eventDetailsArr[4]+
						"<br><strong>Event Desc:</strong> "+eventDetailsArr[5].substring(0,10)+
						"...<input type=\"hidden\" id=\"eventid\" value="+eventDetailsArr[6]+"</div>");
					}
					else if(eventDetailsArr[7] == 'shared' && eventDetailsArr[8] == 'Personal'){
						$(".hidden:contains("+eventDetailsArr[2]+")").after("<div class = \"eventShared\" name=\"Personal\"><strong>Event Name:</strong>"+eventDetailsArr[3]+
						"<br><strong>Event Time:</strong> "+eventDetailsArr[4]+
						"<br><strong>Event Desc:</strong> "+eventDetailsArr[5].substring(0,10)+
						"...<input type=\"hidden\" id=\"eventid\" value="+eventDetailsArr[6]+"</div>");
					}
					else if(eventDetailsArr[7] == 'notshared' && eventDetailsArr[8] == 'Meeting'){
						$(".hidden:contains("+eventDetailsArr[2]+")").after("<div class = \"eventsMeeting\" name = \"Meeting\"><strong>Event Name:</strong>"+eventDetailsArr[3]+
						"<br><strong>Event Time:</strong> "+eventDetailsArr[4]+
						"<br><strong>Event Desc:</strong> "+eventDetailsArr[5].substring(0,10)+
						"...<input type=\"hidden\" id=\"eventid\" value="+eventDetailsArr[6]+"</div>");
					}
					else if(eventDetailsArr[7] == 'notshared' && eventDetailsArr[8] == 'Call'){
						$(".hidden:contains("+eventDetailsArr[2]+")").after("<div class = \"eventsCall\" name = \"Call\"><strong>Event Name:</strong>"+eventDetailsArr[3]+
						"<br><strong>Event Time:</strong> "+eventDetailsArr[4]+
						"<br><strong>Event Desc:</strong> "+eventDetailsArr[5].substring(0,10)+
						"...<input type=\"hidden\" id=\"eventid\" value="+eventDetailsArr[6]+"</div>");
					}
					else if(eventDetailsArr[7] == 'notshared' && eventDetailsArr[8] == 'Personal'){
						$(".hidden:contains("+eventDetailsArr[2]+")").after("<div class = \"eventsPersonal\" name = \"Personal\"><strong>Event Name:</strong>"+eventDetailsArr[3]+
						"<br><strong>Event Time:</strong> "+eventDetailsArr[4]+
						"<br><strong>Event Desc:</strong> "+eventDetailsArr[5].substring(0,10)+
						"...<input type=\"hidden\" id=\"eventid\" value="+eventDetailsArr[6]+"</div>");
					}
				}		
			});
			
				
		}
		
		document.getElementById("nextMonthBtn").addEventListener("click", function(event){
			currentMonth = currentMonth.nextMonth(); // Previous month would be currentMonth.prevMonth()
			$('#meetingsFilter').prop('checked', true);
			$('#callsFilter').prop('checked', true);
			$('#sharedFilter').prop('checked', true);
			$('#personalFilter').prop('checked', true);
			UpdateCalendar(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
			if(username != ''){
				OverlayEvents(); //Also will need to place any events from that month onto the calendar
			}
		}, false);
		
		document.getElementById("prevMonthBtn").addEventListener("click", function(event){
			currentMonth = currentMonth.prevMonth(); // Previous month would be currentMonth.prevMonth()
			$('#meetingsFilter').prop('checked', true);
			$('#callsFilter').prop('checked', true);
			$('#sharedFilter').prop('checked', true);
			$('#personalFilter').prop('checked', true);
			UpdateCalendar(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
			if(username != ''){
				OverlayEvents(); //Also will need to place any events from that month onto the calendar
			}
		}, false);
	   
	   $('#signUp').click(function(){
		   //Brings up the registration pop up
            $('#register').fadeIn(400);
            $('body').append('<div class="mask" id="mask" ></div>');
            $('#mask').fadeIn(400);
			$('#createUser').click(function(){
				var username = $('#username').val();
				var newPassword = $('#password').val();
				var firstName =$('#firstName').val();
				var lastName = $('#lastName').val();
				var email = $('#email').val();
				$.post('registerCalendar.php',{username:username,newPassword:newPassword,firstName:firstName,lastName:lastName,email:email},function(data){
					if(data == 'You have successfully registered. Please log in using the log in button at the top right.'){
						alert(data);
						$('#mask, .popupInfo').fadeOut(400, function(){
							$('#mask').remove();	
						});
					}else{
						alert(data);	
					}
				});
			});
			
        });
       $('#closeReg').click(function(){	
			$('#mask, .popupInfo').fadeOut(400, function(){
				$('#mask').remove();
			});
        });
       $('#signIn').click(function(){
            $('#login').fadeIn(400);
            $('body').append('<div class="mask" id="mask" ></div>');
            $('#mask').fadeIn(400);
			$('#loginButton').click(function(){
				var usernameLogin = $('#usernameLogin').val();
				var passwordLogin = $('#passwordLogin').val();
				$.post('LoginCalendar.php',{username:usernameLogin,password:passwordLogin},function(data){
					var response = data;
					//Split the data coming in on the pound sign. This will allow us to display the user name later.
					var sep = response.split('#');
					$('#usernameLogin').val('');
					$('#passwordLogin').val('');
					if(sep[0] == 'Success'){
						var usernameLoggedIn = sep[1];
						username = usernameLoggedIn;
						token = sep[2];
						//Put welcome message in the top left of the page
						document.getElementById('searchDiv').innerHTML = 'Welcome, ' + usernameLoggedIn;
						//Hide the "sign up" button, replace the "sign in" button with a logout
						$('#signUp').hide();
						$('#signIn').hide();
						$('#signOut').show();
						//$('.signInRegisterDiv').append("<input type=\"button\" class=\"signInSignUpButton\" value=\"Sign Out\" id=\"signOut\" />");
						alert('Thank you for logging in, ' + usernameLoggedIn + '. We will now direct you to your calendar.');
						UpdateCalendar();
						OverlayEvents();
						$('#mask, .popupInfo').fadeOut(400, function(){
							$('#mask').remove();
						});
						
						//Once the user logs in, the calendar becomes clickable as a result of this function	
						$('#calendar').on('click','td',function(){
							$('#dateSelector').fadeIn(400);
							$('body').append('<div class="mask" id="mask" ></div>');
							$('#mask').fadeIn(400);
							
							//If the td is clicked, get the hidden field that has the full date stored in it.
							var dateSelected = $(this).children(":hidden").html();
							$('#eventDate').val(dateSelected);
							//Load events into the delete box for deletion
							$.post("loadSelectorEvents.php",{date:dateSelected, token:token}, function(data){
								$('#eventDelete').html('<option value="select">Select an Event...</option>'+data);	
							});
							//Load users into the shared box
							$.post("loadUsers.php",{token:token},function(data){
								$('#calendarShare').html(data);
							});
						});
						//Add the calendar filters once the user has logged in
						$('#meetingsFilter').change(function(){
							if($("#meetingsFilter").is(":checked")){
								$('[name="Meeting"]').slideDown("slow");
							}else{
								$('[name="Meeting"]').slideUp("slow");
							}
						});
						$('#callsFilter').change(function(){
							if($("#callsFilter").is(":checked")){
								$('[name="Call"]').slideDown("slow");
							}else{
								$('[name="Call"]').slideUp("slow");
							}
						});
						$('#personalFilter').change(function(){
							if($("#personalFilter").is(":checked")){
								$('[name="Personal"]').slideDown("slow");
							}else{
								$('[name="Personal"]').slideUp("slow");
							}
						});
						$('#sharedFilter').change(function(){
							if($("#sharedFilter").is(":checked")){
								$('.eventShared').slideDown("slow");
							}else{
								$('.eventShared').slideUp("slow");
							}
						});
					}else{
						alert(sep[0]);	
					}
					
				});
				
			});
        });
       $('#closeSignIn').click(function(){	
			$('#mask, .popupInfo').fadeOut(400, function(){
				$('#mask').remove();
			});
        });
		//Add listener to the new signout button, bring back the "sign up" and "sign in" buttons if clicked
		$('#signOut').click(function(){
			$.post('logout.php',function(){
				alert('Thank you for logging out. Goodbye.');
				$('#signUp').show();
				$('#signIn').show();
				$('#signOut').hide();
				//$('#signOut').replaceWith("<input type=\"button\" class=\"signInSignUpButton\" value=\"Sign In\" id=\"signIn\" />");
				$(".eventsMeeting, .eventsCall, .eventsPersonal, .eventShared").remove();
				$('#searchDiv').empty();
			});
		});
		$('#addEventButton').click(function(){
			//Get what is entered into the event box
			var eventName = $('#eventName').val();
			var eventDate = $('#eventDate').val();
			var eventTime = $('#eventTime').val();
			var eventDesc = $('#eventDescription').val();
			var eventCat = $('#eventCategory').val();
			var eventShare;
			var shareWithIds;
			//If the user wants to share the event, get the user names they want to share with
			if($('#shareCheckbox').is(':checked')){
				eventShare = $('#shareCheckbox').val();
				shareWithIds = $('#userNameShare').val();
			}else{
				eventShare = '';
				shareWithIds = '';	
			}
			
			$.post('addEvent.php',{eventName:eventName,date:eventDate,time:eventTime,description:eventDesc,eventShare:eventShare,shareWithIds:shareWithIds,eventCat:eventCat,token:token},function(data){
				alert(data);
				OverlayEvents();
				$('#mask, .eventManager').fadeOut(400, function(){
					$('#mask').remove();
				});
			});
			$('#eventName').val('');
			$('#eventTime').val('');
			$('#eventDescription').val('');
			$('#eventCategory').val('');
		});
		$('#cancelAddEvent').click(function(){	
			$('#mask, .eventManager').fadeOut(400, function(){
				$('#mask').remove();
			});
        });
		$('#deleteEventButton').click(function(){
			var eventID = $('#eventDelete').val();
			$.post('deleteEvent.php',{eventID:eventID, token:token},function(data){
				alert(data);
				$('#eventDelete option[value='+eventID+']').remove();
				$('#eventDelete').val('select');
				OverlayEvents();
			});
		});
		//Allows for sharing of entire calendar
		$('#calendarShareButton').click(function(){
			var shareCalendarIds = $('#calendarShare').val();
			$.post('shareCalendar.php',{shareCalendarIds:shareCalendarIds, token:token},function(data){
				alert(data);
				$('#mask, .eventManager').fadeOut(400, function(){
					$('#mask').remove();
				});
			});
		});
		
		$('#shareCheckbox').change(function(){
			$.post("loadUsers.php",{token:token},function(data){
				$('#userNameShare').html(data);
			});
			if($("#userNameSelector").is(":hidden")){
					$("#userNameSelector").slideDown("slow");
			}else{
				$("#userNameSelector").slideUp("slow");
			}
		});
	UpdateCalendar();
    });
	

    </script>
</head>

<body>

<div class="header">
    
    <div class="searchDiv" id = "searchDiv">
        
    </div>
    <div class="signInRegisterDiv">
      <input type="button" class="signInSignUpButton" value="Register" id="signUp" />
      <input type="button" class="signInSignUpButton" value="Sign In" id="signIn" />
      <input type="button" class="signInSignUpButton" value="Sign Out" id="signOut" />
    </div>
</div>
<div class="title">
My Calendar
</div>
<div class="todaydate">
    <table class="topbuttons">
    <tr>
    <td>
    <input type="button" class="signInSignUpButton" value="< Previous" id="prevMonthBtn" />
    </td>
    <td id="currentMonthAndYear">
    </td>
    <td>
     <input type="button" class="signInSignUpButton" value="Next >" id="nextMonthBtn" />
    </td>
    </tr>
    </table>
</div>

<div class="container">
    <div id = "register" class="popupInfo">
        <form method="post" name="createUser">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Register</h1></td></tr>
            <tr><td>User Name</td><td><input type="text" name = "newUserName" id="username" class = "tField" /></td></tr>
            <tr><td>Password</td><td><input type="password" name = "newPassword" id = "password" class = "tField" /></td></tr>
            <tr><td>First Name</td><td><input type="text" name = "firstName" id = "firstName" class = "tField" /></td></tr>
            <tr><td>Last Name</td><td><input type="text" name = "lastName" id = "lastName" class = "tField" /></td></tr>
            <tr><td>Email</td><td><input type="text" name = "email" id = "email" class = "tField" /></td></tr>
            <tr><td><input type="button" name="createUser" id="createUser" class="signInSignUpButton" value="Register" /> </td>
            <td><input type="button" id ="closeReg" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>
    <div id = "login" class="popupInfo">
        <form method="post" name="loginUser">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Sign In</h1></td></tr>
            <tr><td>User Name</td><td><input type="text" name = "username" id="usernameLogin" class = "tField" /></td></tr>
            <tr><td>Password</td><td><input type="password" name = "passwordEntered" id = "passwordLogin" class = "tField" /></td></tr>
            <tr><td><input type="button" name="Login" id="loginButton" class="signInSignUpButton" value="Login" /> </td>
            <td><input type="button" id ="closeSignIn" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>
    
    <div id = "dateSelector" class="eventManager">
        <h1 class="titleSmall">Event Manager</h1>
        <div class="closeButton"><input type="button" id ="cancelAddEvent" class="signInSignUpButton" value="Close" /></div><br>
        <div class = "addEvent">
        <form method="post" name="addEvent">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Add an Event</h1></td></tr>
            <tr><td>Event Name</td><td><input type="text" name = "eventName" id="eventName" class = "tField" /></td></tr>
            <tr><td>Event Date</td><td><input type="text" name = "eventDate" id="eventDate" class = "tField" readonly="readonly" /></td></tr>
            <tr><td>Event Time</td><td><input type="time" name = "eventTime" id="eventTime" class = "tField" /></td></tr>
            <tr><td>Event Description</td><td><textarea name = "eventDescription" id="eventDescription" ></textarea></td></tr>
            <tr><td>Event Category</td><td><select name = "eventCategory" id="eventCategory" class = "deleteEventSelector" >
            <option value="Meeting">Meeting</option><option value="Call">Call</option><option value="Personal">Personal</option></select></td></tr>
            <tr><td>Share With Other Users?</td><td><input type="checkbox" name = "username" id="shareCheckbox" value="yes" /><div id = "userNameSelector" class="userNameSelectorDiv"><select name = "userNameShare" id="userNameShare" class="userNameSelector" multiple></select></div></td></tr>
            <!--<tr><td colspan="2"></td></tr>-->
            <tr><td colspan="2"><input type="button" name="addEvent" id="addEventButton" class="signInSignUpButton" value="Add Event" /> </td></tr>
        </table>
        </form>
        </div>
        <div class = "deleteEvent">
        <form method="post" name="addEvent">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Delete An Event</h1></td></tr>
            <tr><td>Event Name</td><td><select name = "eventDelete" id="eventDelete" class="deleteEventSelector"></select></td></tr>
        	<tr><td colspan="2"><input type="button" name="deleteEvent" id="deleteEventButton" class="signInSignUpButton" value="Delete Event" /></td></tr>
        </table>
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Share Entire Calendar</h1></td></tr>
            <tr><td>Share With:</td><td><select name = "calendarShare" id="calendarShare" class="userNameSelector" multiple></select></td></tr>
        	<tr><td colspan="2"><input type="button" name="calendarShareButton" id="calendarShareButton" class="signInSignUpButton" value="Share Calendar" /></td></tr>
        </table>
        </form>
        </div>
    </div>

    <table class = "calendar" id = "calendar">
        
    </table>
    <div class="filters">
    <table class="table">
    	<tr><td colspan="8">Apply Filters to the Calendar</td></tr>
        <tr><td>Meetings:</td><td><input type="checkbox" name = "meetingsCheck" id="meetingsFilter" value="yes" checked/></td>
        <td>Calls:</td><td><input type="checkbox" name = "meetingsCheck" id="callsFilter" value="yes" checked/></td>
        <td>Personal:</td><td><input type="checkbox" name = "meetingsCheck" id="personalFilter" value="yes" checked/></td>
        <td>Events Shared with You:</td><td><input type="checkbox" name = "sharedFilter" id="sharedFilter" value="yes" checked/></td>
        </tr>
    </table>
    
    </div>
    
</div>



</body>
</html>
