//Some Code From: https://github.com/hwz/chirp

var app = angular.module('logARunApp', ['ngRoute', 'ngResource']).run(function($rootScope, $http) {
	$rootScope.authenticated = false;
	$rootScope.user_currently_logged_in = '';
	$rootScope.dateSelected = '';
	$rootScope.dateFiltered = '';
	
	$rootScope.signout = function(){
    	$http.get('/signout');
    	$rootScope.authenticated = false;
    	$rootScope.user_currently_logged_in = '';
	};
});

app.config(function($routeProvider){
	$routeProvider
		//the timeline display
		.when('/home', {
			templateUrl: 'main.html',
			controller: 'newsFeedController'
		})
		.when('/calendar',{
			templateUrl: 'calendar.html',
			controller: 'calController'
		})
		.when('/todaysRun',{
			templateUrl: 'logForm.html',
			controller: 'loggingController'
		})
		.when('/todaysRun/:fullDate/:username?',{
			templateUrl: 'logForm.html',
			controller: 'loggingController'
			 
		})

		//the login display
		.when('/login', {
			templateUrl: 'login.html',
			controller: 'authController'
		})
		//the signup display
		.when('/register', {
			templateUrl: 'register.html',
			controller: 'authController'
		})
		.otherwise({redirectTo: '/home'});
	;
});

app.factory('loggingService', function($resource){
	return $resource('/api/todaysRun/:fullDate/:username?');
});

app.factory('loggingServicev2', function($resource){
	return $resource('/api/todaysRun/:fullDate/:id',{ id: '@_id' }, {
	    update: {
	      method: 'PUT'
	    }
	});
});

app.factory('calWeek', function($http){
	var Week = function(initial_day){
		this.sunday = initial_day.getSunday();
	}
	
	Week.prototype.nextWeek = function(){
		return new Week(this.sunday.deltaDays(7));
	}

	Week.prototype.prevWeek = function () {
		return new Week(this.sunday.deltaDays(-7));
	};
	
	Week.prototype.contains = function (d) {
		return (this.sunday.valueOf() === d.getSunday().valueOf());
	};

	Week.prototype.getDates = function () {
		var dates = [];
		for(var i=0; i<7; i++){
			dates.push(this.sunday.deltaDays(i));
		}
		return dates;
	};

	return Week;

});

app.factory('calMonth', function($http, $q, calWeek){
	var Month = function(year, month){
		this.year = year;
		this.month = month;
	}

	Month.prototype.nextMonth = function(){
		var self = this;
		return new Month(self.year+Math.floor((self.month+1)/12), (self.month+1)%12);
	}

	Month.prototype.prevMonth = function(){
		var self = this
		return new Month(this.year+Math.floor((this.month-1)/12),(this.month+11)%12);
	}

	Month.prototype.getdateObject = function(d){
		return new Date(this.year, this.month, d);
	}

	Month.prototype.getWeeks = function(){
		var firstDay = this.getDateObject(1);
		var lastDay = this.nextMonth().getDateObject(0);
		
		var weeks = [];
		var currweek = new Week(firstDay);
		weeks.push(currweek);
		while(!currweek.contains(lastDay)){
			currweek = currweek.nextWeek();
			weeks.push(currweek);
		}
		
		return weeks;
	}

	return Month; 
	
});

app.controller('calController', function($scope, $rootScope, $location, calMonth, calWeek){
	
	Date.prototype.deltaDays = function(numDiff){
		return new Date(this.getFullYear(), this.getMonth(), this.getDate()+numDiff);
	}
	Date.prototype.getSunday = function(){
		return this.deltaDays(-1*this.getDay());
	}

	$scope.date = new Date();
	$scope.currentMonth = new Month($scope.date.getFullYear(), $scope.date.getMonth());
	$scope.weeksArr = $scope.currentMonth.getWeeks();
	//$scope.dateSelected = '';
	$scope.updateCalendar = function(){
		

	}

	$scope.nextMonthControl = function(){
		$scope.currentMonth = $scope.currentMonth.nextMonth();
		$scope.weeksArr = $scope.currentMonth.getWeeks();
		//The following used for troubleshooting
		//var curr = $scope.currentMonth;
		//var monthNumber = curr.getDateObject(1).getMonth();
		//alert(monthNumber);
	}

	$scope.prevMonthControl = function(){
		$scope.currentMonth = $scope.currentMonth.prevMonth();
		$scope.weeksArr = $scope.currentMonth.getWeeks();
		//The following used for troubleshooting
		//var curr = $scope.currentMonth;
		//var monthNumber = curr.getDateObject(1).getMonth();
		//alert(monthNumber);
	}

	$scope.createLog = function(day){
		//alert(day);
		$rootScope.dateSelected = day;
		var month = $rootScope.dateSelected.getMonth();
		var day = $rootScope.dateSelected.getDate();
		var year = $rootScope.dateSelected.getFullYear();
		var dateString = month+ '-' + day + '-' + year;
		//alert($scope.dateSelected);
		$location.path('/todaysRun');
		
		//alert($scope.dateSelected);
	}	

});

app.controller('loggingController', function(loggingService, loggingServicev2, $scope, $rootScope, $routeParams, $location, $http){
	var month = $rootScope.dateSelected.getMonth();
	var day = $rootScope.dateSelected.getDate();
	var year = $rootScope.dateSelected.getFullYear();
	$scope.fullDate = month+ '-' + day + '-' + year;
	
	$scope.dateSelectedAgain = $routeParams.dateSelected;
	//$scope.logs = loggingService.query();
	$scope.newLog = {username: '', mileage: '', timeMinutes: '', pace: '', description: '', comments: '', date: ''};
	//$scope.updateLog = {username: '', mileage: '', timeMinutes: '', pace: '', description: '', comments: '', date: ''};
	
	var logs = loggingService.query({fullDate: $scope.fullDate, username: $rootScope.user_currently_logged_in}, function(){
		$scope.logs = logs;
	});

	$scope.insertNewLog = function() {
	  $scope.newLog.username = $rootScope.user_currently_logged_in;
	  var month = $rootScope.dateSelected.getMonth();
	  var day = $rootScope.dateSelected.getDate();
	  var year = $rootScope.dateSelected.getFullYear();
	  $scope.newLog.date =  month+ '-' + day + '-' + year;
	  $scope.newLog.pace = $scope.newLog.timeMinutes / $scope.newLog.mileage;
	  console.log($scope.newLog.pace); 
	  loggingService.save($scope.newLog, function(){
	    $scope.logs = loggingService.query({fullDate: $scope.fullDate, username: $rootScope.user_currently_logged_in });
	    $scope.newLog = {username: '', mileage: '', timeMinutes: '', pace: '', description: '', comments: '', date: ''};
	  });
	  
	};

	$scope.delete = function(id) {
		var inputId = id;
		loggingServicev2.delete({fullDate: $scope.fullDate, id: inputId});
		$scope.logs = loggingService.query({fullDate: $scope.fullDate});
	};

	$scope.update = function(id){
		var inputId = id;
		loggingServicev2.delete({fullDate: $scope.fullDate, id: inputId});
		$scope.newLog.username = $rootScope.user_currently_logged_in;
	  	var month = $rootScope.dateSelected.getMonth();
	  	var day = $rootScope.dateSelected.getDate();
	  	var year = $rootScope.dateSelected.getFullYear();
	  	$scope.newLog.date =  month+ '-' + day + '-' + year;
	  	$scope.newLog.pace = $scope.newLog.timeMinutes / $scope.newLog.mileage;
	  	
	  	loggingService.save($scope.newLog, function(){
	   		$scope.logs = loggingService.query({fullDate: $scope.fullDate});
	    	$scope.newLog = {username: '', mileage: '', timeMinutes: '', pace: '', description: '', comments: '', date: ''};
	  	});
		
	}



	  /*
	  $scope.newLog.username = $rootScope.user_currently_logged_in;
	  $scope.newLog.date = $rootScope.dateSelected;

	  loggingService.save($scope.newLog, function(){
	    $scope.logs = loggingService.query();
	    $scope.newLog = {username: '', mileage: '', timeMinutes: '', pace: '', description: '', comments: '', date: ''};
	  });
	  alert('Success');
	  //$location.path('/calendar');
	  */
});

app.controller('newsFeedController', function(loggingService, $scope){
	$scope.newsLogs = loggingService.query();
	
});



//Main page controllers

//NEED TO ADD 'logControllers' to app.js


//begin RSS feed: Source: http://jsfiddle.net/mahbub/b8Wcz/


app.factory('FeedService',['$http',function($http){
    return {
        parseFeed : function(url){
            return $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));
        }
    };
}]);

app.controller('FeedCtrl', ['$scope','FeedService', function ($scope,Feed) {    
    $scope.loadButtonText="Load";
    $scope.loadFeed=function(e){        
        Feed.parseFeed($scope.feedSrc).then(function(res){
            $scope.loadButtonText=angular.element(e.target).text();
            $scope.feeds=res.data.responseData.feed.entries;
            alert($scope.feeds);
        });
    };
}]);



/*
app.controller("leaderboardController", function($scope, $rootScope) {
      $scope.
      self.orderProp = 'weeklyMiles';
      $http.get('asdfasdf/runners.json').then(function(response) {
          self.runners = response.data;
      });
}]);
*/


//Controller to determine authorization

app.controller('authController', function($scope, $http, $rootScope, $location){
  $scope.user = {username: '', password: ''};
  $scope.error_message = '';

  $scope.login = function(){
    $http.post('/auth/login', $scope.user).success(function(data){
      if(data.state == 'success'){
        $rootScope.authenticated = true;
        $rootScope.user_currently_logged_in = data.user.username;
        $location.path('/');
      }
      else{
        $scope.error_message = data.message;
      }
    });
  };

  $scope.register = function(){
    $http.post('/auth/signup', $scope.user).success(function(data){
      if(data.state == 'success'){
        $rootScope.authenticated = true;
        $rootScope.user_currently_logged_in = data.user.username;
        $location.path('/');
      }
      else{
        $scope.error_message = data.message;
      }
    });
  };
});



