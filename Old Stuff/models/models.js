var mongoose = require('mongoose');

var postSchema = new mongoose.Schema({
	text: String,
	username: String,
	created_at: {type: Date, default: Date.now}
});

var userSchema = new mongoose.Schema({
    username: String,
    password: String, //hash created from password
    created_at: {type: Date, default: Date.now}
});

//declare model called User which has schema userSchema
mongoose.model("Post", postSchema);
mongoose.model("User", userSchema);

