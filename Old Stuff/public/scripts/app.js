var app = angular.module('logARunApp', ['ngRoute']).run(function($rootScope){
	$rootScope.authenticated = false;
	$rootScope.current_user = "";

	$rootScope.logout = function(){
		$http.get('auth/signout');

		$rootScope.audthenticated = false;
		$rootScope.current_user = "";
	};
});

app.config(function($routeProvider){
	$routeProvider
		//the timeline display
		.when('/', {
			templateUrl: 'main.html',
			controller: 'mainController'
		})
		//the login display
		.when('/login', {
			templateUrl: 'login.html',
			controller: 'authController'
		})
		//the signup display
		.when('/register', {
			templateUrl: 'register.html',
			controller: 'authController'
		});
});

app.factory('postService', function($http){
	var factory = {};
	factory.getAll = function(){
		return $http.get('/api/posts');
	}
});
app.controller('mainController', function($scope, postService){
	$scope.posts = []; //this will display all posts on the home page
	$scope.newPost = {create_by: '', text: '', create_at: ''};

	postService.getAll().success(function(data){
		$scope.posts = data;
	});

	$scope.post = function(){
		$scope.newPost.created_at = Date.now(); //timestampe of the post
		$scope.posts.push($scope.newPost); //Push onto the posts array
		$scope.newPost = {created_by: '', text: '', created_at: ''}; //Reset new post

	};

});

app.controller('authController', function($scope, $rootScope, $http, $location){
	$scope.user = {username: '', password: ''};
	$scope.error_message = '';


	$scope.login = function(){

		$http.post('/auth/login', $scope.user).success(function(data){
			if(data.state == 'success'){
				$rootScope.authenticated = true;
				$rootScope.current_user = data.user.username;

				$location.path('/');
			}else{
				console.log('poop');
				$scope.error_message = data.message;
			}
		});

	};

	$scope.register = function(){
		$http.post('/auth/register', $scope.user).success(function(data){
			$rootScope.authenticated = true;
			$rootScope.current_user = data.user.username;

			$location.path('/');
		});
		
	};
});