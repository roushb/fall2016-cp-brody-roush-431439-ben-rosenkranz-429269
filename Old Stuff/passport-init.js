var LocalStrategy   = require('passport-local').Strategy;
var passport = require('passport');
var bCrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
//temporary data store
//var users = {}; //Temporarily using the local memory, in future will use MongoDB
module.exports = function(passport){

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
        //Tell passport what to use for user
        console.log('serializing user:',user._id);
        return done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
    	//Returns the user object back
    	User.findById(id, function(err, user){
    		
    		if(err){
    			return done(err, false);
    		}

    		if(!user){
    			return done('user not found', false);
    		}
			//found user object provide it back to passport
			return done(user, true);
    	});
        

    });

    passport.use('login', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) { 
        	
        	User.findOne({username: username}, function(err, user){
        		if(err){
        			return done(err, false);
        		}

        		if(!user){
        			//there is no user with this username
        			return done(null, false, {message: 'Incorrect username'});
        		}

        		if(!isValidPassword(user, password)){
        			//wrong password
        			return done(null, false, {message: 'Incorrect password'});
        		}
        		console.log('Successful signin by ' + user.username);
        		return done(null, user);
        	});
        	/*
        	//Check if user exists
        	if(!users[username]){
        		console.log('User not found with username ' + username);
        		return done(null, false);
        	}
        	//check if password is correct
        	if(isValidPassword(users[username], password)){
        		return done(null, users[username]);
        	}else{
        		//Successfully signed in
	        	console.log('Invalid password ' + username);
	            return done(null, false);
        	}
        	*/

        }
    ));

    passport.use('signup', new LocalStrategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
        	User.findOne({username: username}, function(err, user){
        		if(err){
        			return done(err, false);
        		}

        		if(user){
        			//user already signed up
        			return done('username already taken', false);
        		}

        		var user = new User();

        		user.username = username;
        		user.password = createHash(password); 

        		user.save(function(err, user){
        			if(err){
        				return done(err, false);
        			}
        			console.log('successfully signed up user ' + username);
        			return done(null, user);
        		})


        	});
        	//check if the user already exists
        	/*if(users[username]){
        		console.log('User already exists with username ' + username)
        		return done(null, false);
        	}

        	//automatically parses out the form body and everything
        	users[username] = {
        		username: username,
        		password: createHash(password) 
        	};
        	console.log(users[username].username + ' Registration successful');
            return done(null, users[username]);*/

        })
    );
    //The following uses the bcrypt API to salt passwords
    var isValidPassword = function(user, password){
        return bCrypt.compareSync(password, user.password);
    };
    // Generates hash using bCrypt
    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    };

};