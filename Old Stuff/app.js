var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');

var session = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//Initialize models
require('./models/models.js');
var index = require('./routes/index');
var api = require('./routes/api');

var mongoose = require('mongoose');
//connect to mongodb
mongoose.connect("mongodb://localhost:27017/chirp-test");
//Allegedly supposed to come after mongoose
var app = express();
var passport = require('passport');
var authenticate = require('./routes/authenticatev2')(app, passport);



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(session({
	secret: 'super duper secret'
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session()); //This creates a unique has for our session


//Initialize Passport
var initPassport = require('./passport-init');
initPassport(passport);

app.use('/', index);
app.use('/auth', authenticate);
app.use('/api', api);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  /**/
  //res.render('errors/500', {error: err, stack: err.stack});
  // set locals, only providing error in development
  if(!err){
  	console.log('The damn error is undefined');
  	return next();
  }else{
	  res.locals.message = err.message;
	  res.locals.error = req.app.get('env') === 'development' ? err : {};
	  // render the error page
	  res.status(err.status || 500);
        res.render('error', {
            message: err.message, 
            error: err
        });
  }
});

module.exports = app;
