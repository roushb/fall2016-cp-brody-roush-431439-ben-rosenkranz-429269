var express = require('express');
//var app = express.app();

module.exports = function(passport, app){

    //sends successful login state back to angular
    app.post('/success', function(req, res){
        console.log('successful');
        res.send({state: 'success', user: req.user ? req.user : null});
    });

    //sends failure login state back to angular
    app.get('/failure', function(req, res){
        res.send({state: 'failure', user: null, message: "Invalid username or password"});
    });

    //log in
    app.post('/login', passport.authenticate('login', { failureRedirect: '/auth/failure' }),
        function(req, res) {
            //res.redirect('/auth/success');
            res.send({state: 'success', user: req.user ? req.user : null});
        });
    

    //sign up
    app.post('/register', passport.authenticate('register', {
        successRedirect: '/auth/success',
        failureRedirect: '/auth/failure'
    }));

    //log out
    app.get('/signout', function(req, res) {
        console.log('Signing out...');
        req.session.destroy(function (err) {
            res.redirect('/auth/success'); //Inside a callback… bulletproof!
        });
    });

    //return app;

}