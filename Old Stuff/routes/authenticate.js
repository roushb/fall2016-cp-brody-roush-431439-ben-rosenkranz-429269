var express = require('express');
var router = express.Router();

module.exports = function(passport){

    //sends successful login state back to angular
    router.post('/success', function(req, res){
        console.log('successful');
        res.send({state: 'success', user: req.user ? req.user : null});
    });

    //sends failure login state back to angular
    router.get('/failure', function(req, res){
        res.send({state: 'failure', user: null, message: "Invalid username or password"});
    });

    //log in
    router.post('/login', passport.authenticate('login', { failureRedirect: '/auth/failure' }),
        function(req, res) {
            //res.redirect('/auth/success');
            res.send({state: 'success', user: req.user ? req.user : null});
        });
    

    //sign up
    router.post('/register', passport.authenticate('register', {
        successRedirect: '/auth/success',
        failureRedirect: '/auth/failure'
    }));

    //log out
    router.get('/signout', function(req, res) {
        console.log('Signing out...');
        req.session.destroy(function (err) {
            res.redirect('/auth/success'); //Inside a callback… bulletproof!
        });
    });

    return router;

}