var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Post = mongoose.model('Post');

router.use(function(req, res, next){
	//Allows all public users to read posts, regardless of whether or not logged in - will have to manage this for private accounts
	//Continue to the next middleware or request handler
	if(req.method === "GET"){
		return next();
	}	
	
	if(req.isAuthenticated()){
		//user not authenticated, redirect to login page
		return next();
	}
	//user authenticated, continue to next middleware or handler
	
	return res.redirect('/#login');
});
router.route('/posts')
	//returns all posts
	.get(function(req, res){
		Console.log('Posts...')
		Post.find(function(err,data){
			if(err){
				console.log(err);
				return res.send(500, err);
			}

			return res.send(data);

		});
		//temporary solution
		//res.send({message: 'TODO return all posts'});
	})

	.post(function(req,res){
		console.log('Posting...');
		var post = new Post();
		post.text = req.body.text;
		post.username = req.body.created_by;
		post.save(function(err, post){
			if(err){
				console.log(err);
				return res.send(500, err);
			}
			return res.json(post);
		});

		//temporary stolution
		//res.send({message: 'TODO create a new post'});
	});

router.route('/posts/:id')
//CRUD --> Create, Read, Update, Delete --> This is our Restful API
	//returns a specific post
	.get(function(req, res){
		Post.findById(req.params.id, function(err, post){
			if(err){
				res.send(err);
			}
			res.json(post);
		})
	})

	//updates exisiting post
	.put(function(req,res){
		Post.findById(req.params.id, function(err, post){
			if(err){
				res.send(err);
			}

			post.username = req.body.created_by;
			post.text = req.body.text;

			post.save(function(err, post){
				if(err){
					res.send(err);
				}

				res.json(post);
			});
		});	
	})
	//deletes existing post
	.delete(function(req,res){
		Post.remove({
			_id: req.params.id
		}, function(err){
			if(err){
				res.send(err);
			}
			res.json("deleted :(");	
		});
	}); 

module.exports = router;