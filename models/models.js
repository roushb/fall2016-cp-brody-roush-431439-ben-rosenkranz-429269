var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Create schema to insert daily logs into database
var dailyLogSchema = new mongoose.Schema({
  username: String,
  mileage: Number,
  timeMinutes: Number,
  pace: Number,
  description: String,
  comments: [{ body: String, date: Date }],
  date: String
});

/*
dailyLogSchema.methods.calculatePace = function(){
    this.pace = this.timeMinutes / this.mileage;
    return this. pace;
};
*/

var runnerSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  //comments: [{ body: String, date: Date }],
  dateJoined: { type: Date, default: Date.now },
  dateUpdated: { type: Date, default: Date.now },
  hidden: { type: Boolean, default: false },
  admin: { type: Boolean, default: false }
});


mongoose.model('dailyLogModel', dailyLogSchema);
mongoose.model('Runner', runnerSchema);
