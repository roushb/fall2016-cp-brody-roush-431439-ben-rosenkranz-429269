The requirements are as follows:
 # General 
 Site is implemented on the MEAN stack	15
-# User Features:	
-Calendar functions as it Should	5
-Users can add workouts to the calendar	5
-Users can edit workouts	2
-Users can delete workouts	3
-Pace is automatically calculated for appropriate workouts	3
-Users can join teams	5
-Users can edit privacy settings	5
-	
-# Team/Site Features:	
-All team users are agreggated onto one page	5
-Users can comment on other users logs, based on privacy settings	7
-Users receive alerts (like facebook) when someone has commented on their log	10
-Users can search for other users	2
-Site is visually appealing	4
-Code is well formatted and easy to read, with proper commenting	2
-Code passes HTML validation	2
-Creative	20
-Total	95

UPDATED README
-Rubric re-approved by Jake Kent, 12/5

Log In/Sign out/ registration 15
 
Create Calendar using AngularJS by implementing $resource, factories, and controllers 20
Users can add, edit, delete workouts to their calendar 12
Leaderboard of top ten mileage appear on home page 5
Site is visually appealing 5
Code is well formatted 2
Code passes HTML Validation 2
 
Rubric 5
 
Creative 20
News feed on home page loads in all users' recent posts 5
RSS Feed of Recent Running News 5 <br>
Pace is calculated for each run 3

#Citation
We owe a thanks to the people at Microsoft, whose tutorial we integrated into parts of this project. https://github.com/hwz/chirp